/**
 * This program waits for child process and "reaps" them, i.e. read
 * off their status so that they can terminate. The program exits when
 * it runs out of children.
 */
#include <signal.h>
#include <string.h>
#include <sys/wait.h>

int main(void) {
    sigset_t set;
    siginfo_t status;

    sigfillset(&set);
    sigprocmask(SIG_BLOCK,&set,NULL);

    do {
	memset( &status, 0, sizeof( status ) );
    } while ( waitid( P_ALL, 0, &status, WEXITED ) == 0 );
    return 0;
}
