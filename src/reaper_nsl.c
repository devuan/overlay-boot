//#include <errno.h>
//#include <signal.h>
//#include <string.h>
//#include <unistd.h>
//#include <sys/wait.h>
//#include <sys/types.h>

//int main(void) {
//	sigset_t set;
//	siginfo_t status;
//
//	if (getpid()!=1)
//		return 1;
//	sigfillset(&set);
//	sigprocmask(SIG_BLOCK,&set,NULL);
//	memset(&status,0,sizeof status);
//	while (-ECHILD!=waitid(P_ALL,0,&status,WEXITED));
//	return 1;
//}

#define _GNU_SOURCE

#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <linux/unistd.h>

static inline void nsl_exit1(void) {
	asm("mov $60,%eax");
	asm("mov $1,%rdi");
	asm("syscall");
}

__attribute__((naked)) void _start(void) {
	nsl_exit1();
}
