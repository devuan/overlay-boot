#!/bin/sh /lib/init/init-d-script
### BEGIN INIT INFO
# Provides:          devices
# Required-Start:    
# Required-Stop:     
# Default-Start:     S
# Default-Stop:      0 1 6
# Short-Description: set up devnodes
# Description:       Initialize /dev from /.devnodes.tgz
### END INIT INFO

DAEMON=none
DESC="devnodes: restore and capture /dev on startup and shutdown."
TGZ=/.devnodes.tgz

do_start_override() {
    [ -e $TGZ ] && tar -xzf $TGZ --skip-old-files -C /dev
}

do_stop_override() {
    cd /dev && tar czf $TGZ *
}

do_status_override() {
    find /dev -type d -exec ls -ld '{}' ';'
    stat -c %Y $TGZ
}
