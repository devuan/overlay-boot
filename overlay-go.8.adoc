overlay-go(8)
=============
:doctype: manpage
:revdate: {sys:date "+%Y-%m-%d %H:%M:%S"}
:COLON: :
:EQUALS: =

NAME
----
overlay-go - Start a shell within a subhost namespace environment.

SYNOPSIS
--------
*overlay-go* _name_

DESCRIPTION
-----------
*overlay-go* is an adminstration utility for entering the namespace
environment of a "booted" subhost started witb +overlay-boot+. This
starts a +bash+ shell within the subhost namespace, though not a child
of its pid 1.

OPTIONS
-------

no options.

EXAMPLES
--------

====
----
overlay-go tiny
----
====

SEE ALSO
--------

*overlay-boot*, *overlay-stop*
