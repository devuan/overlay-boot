overlay-share(8)
================
:doctype: manpage
:revdate: {sys:date "+%Y-%m-%d %H:%M:%S"}
:COLON: :
:EQUALS: =
:BANG: !

NAME
----
overlay-share - Utility to bind mount directory into subhost.

SYNOPSIS
--------
*overlay-share* _path_ _subhostname_

DESCRIPTION
-----------

*overlay-share* is a utility command script for bind-mounting a
 directory into a subhost under the same pathname.

OPTIONS
-------

no options.

EXAMPLES
--------

====
----
# overlay-share /home/guest example
----
====

The above bind-mounts the main host's directory tree +/home/guest+
into the subhost +example+ and thereby making that directory subtree
shared between the subhost and the main host.

SEE ALSO
--------

*overlay-boot*, *overlay-stop*
